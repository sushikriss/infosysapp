# Organization Management Application

## About
This project involves the development of the back-end part of a web application for maintaining information about the hierarchical structure of an organization consisting of directorates, departments, and employees.

![diagram](organization-management/src/main/resources/static/Structure.png)

## Feautures

1. **Authentication**: The application only allows access to authenticated users. For security, Spring Security is used with common authentication algorithms.

2. **APIs**: For each of the three registers (directorate, department, and employee), APIs are implemented through which a user can search, create, edit, and disable records.

## Domain Model

The domain model consists of objects: “Employee”, “Department”, “Directorate”. 

1. **Directorate**: Directorates may consist of several Departments. Each Directorate has exactly one Employee who is the “Director of the Directorate”. 

2. **Department**: Each Department can have an unlimited number of Employees. All Departments have a hierarchical structure on **2 levels**: “Head of Department” and “Employee”. One Employee **can't be** in two Departments at the same time. 

- Each directorate and each department has a “Name” and a “Description”.

3. **Employee**: Each Employee has the following characteristics: Name, Surname, Personal identification number, Age, Position (values are from the nomenclature).

4. **Roles and rights**: Employee can be either regular Employee, Head of Department or Director of Directorate if set by the Position/Role.

**“Head of Department”** has access to details of the Employees in his management Dept.

**“Director of Directorate”** has access to information about Employees in all Departments in the Directorate headed by him.

**Admin**: Admin has the privilege to create new Directorates and Departments. This ensures that the hierarchical structure of the organization is maintained and managed by a central authority. 

- Any changes to the Directorates or Departments are logged and audited for transparency and accountability.