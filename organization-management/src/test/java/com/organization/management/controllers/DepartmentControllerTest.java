package com.organization.management.controllers;

import com.organization.management.core.enums.PositionEnum;
import com.organization.management.web.dto.department.DepartmentFormDTO;
import com.organization.management.web.dto.employee.EmployeeDTO;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DepartmentControllerTest {
  @LocalServerPort
  private int port;

  @Before
  public void setup() {
    RestAssured.baseURI = "http://localhost";
    RestAssured.port = port;
  }

  @Test
  public void whenGetDepartment_thenOk() {
    given()
        .auth()
        .basic("admin", "admin")
        .when()
        .get("/api/department/{id}", 1L)
        .then()
        .statusCode(200);
  }

  @Test
  public void whenCreateDepartment_thenCreated() {
    DepartmentFormDTO departmentFormDTO = createDummyDepartmentFormDTO();

    given()
        .auth()
        .basic("admin", "admin")
        .contentType(ContentType.JSON)
        .body(departmentFormDTO)
        .when()
        .post("/api/department")
        .then()
        .statusCode(200);
  }

  private DepartmentFormDTO createDummyDepartmentFormDTO() {
    EmployeeDTO employeeDTO = new EmployeeDTO();
    employeeDTO.setUsername("employee3");
    employeeDTO.setPosition(PositionEnum.HEAD_OF_DEPARTMENT.name());

    DepartmentFormDTO departmentDTO = new DepartmentFormDTO();
    departmentDTO.setName("VALID_DIRECTORATE");
    departmentDTO.setDescription("VALID_DESCRIPTION");
    departmentDTO.setHeadOfDepartment(employeeDTO);
    return departmentDTO;
  }
}
