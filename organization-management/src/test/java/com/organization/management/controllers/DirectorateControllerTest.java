package com.organization.management.controllers;

import com.organization.management.core.enums.PositionEnum;
import com.organization.management.web.dto.directorate.DirectorateDTO;
import com.organization.management.web.dto.directorate.DirectorateFormDTO;
import com.organization.management.web.dto.employee.EmployeeCreateDTO;
import com.organization.management.web.dto.employee.EmployeeDTO;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DirectorateControllerTest {
  @LocalServerPort
  private int port;

  @Before
  public void setup() {
    RestAssured.baseURI = "http://localhost";
    RestAssured.port = port;
  }

  @Test
  public void whenGetDirectorate_thenOk() {
    given()
        .auth()
        .basic("admin", "admin")
        .when()
        .get("/api/directorate/{id}", 1L)
        .then()
        .statusCode(200);
  }

  @Test
  public void whenCreateDirectorate_thenCreated() {
    EmployeeDTO employeeDTO = new EmployeeDTO();
    employeeDTO.setUsername("employee3");
    employeeDTO.setPosition(PositionEnum.DIRECTOR_OF_DIRECTORATE.name());

    DirectorateFormDTO directorateDTO = new DirectorateFormDTO();
    directorateDTO.setName("ANOTHER_DIRECTORATE");
    directorateDTO.setDescription("ANOTHER_DIRECTORATE");
    directorateDTO.setDirectorOfDirectorate(employeeDTO);

    given()
        .auth()
        .basic("admin", "admin")
        .contentType(ContentType.JSON)
        .body(directorateDTO)
        .when()
        .post("/api/directorate")
        .then()
        .statusCode(200);
  }

  @Test
  @DirtiesContext
  public void whenUpdateDirectorate_thenOk() {
    DirectorateFormDTO directorateDTO = createDummyForUpdatingExistingDirectorate();

    given()
        .auth()
        .basic("admin", "admin")
        .contentType(ContentType.JSON)
        .body(directorateDTO)
        .when()
        .put("/api/directorate/{id}", 1L)
        .then()
        .statusCode(200);
  }

  @Test
  public void whenDeleteDirectorate_thenOk() {
    given()
        .auth()
        .basic("admin", "admin")
        .when()
        .delete("/api/directorate/{id}", 2L)
        .then()
        .statusCode(200);
  }

  private DirectorateFormDTO createDummyForUpdatingExistingDirectorate() {
    EmployeeDTO employeeDTO = new EmployeeDTO();
    employeeDTO.setName("employee4");
    employeeDTO.setUsername("employee4");
    employeeDTO.setPosition(PositionEnum.DIRECTOR_OF_DIRECTORATE.name());

    DirectorateFormDTO directorateDTO = new DirectorateFormDTO();
    directorateDTO.setName("FIRST_DIRECTORATE");
    directorateDTO.setDescription("FIRST_DIRECTORATE");
    directorateDTO.setDirectorOfDirectorate(employeeDTO);
    return directorateDTO;
  }
}
