package com.organization.management.controllers;

import com.organization.management.core.enums.PositionEnum;
import com.organization.management.web.dto.employee.EmployeeCreateDTO;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class EmployeeCreationControllerTest {
  @LocalServerPort
  private int port;

  @Before
  public void setup() {
    RestAssured.baseURI = "http://localhost";
    RestAssured.port = port;
  }

  @Test
  public void whenCreateEmployeeAsDirector_thenCreated() {
    EmployeeCreateDTO createDTO = createDummyEmployeeDTO();

    given()
        .auth()
        .basic("directorOne", "directorOne")
        .contentType(ContentType.JSON)
        .body(createDTO)
        .when()
        .post("/api/employee/director-create")
        .then()
        .statusCode(201);
  }

  @Test
  public void whenCreateEmployeeAsHead_thenCreated() {
    EmployeeCreateDTO createDTO = createDummyEmployeeDTO();
    createDTO.setUsername("TotallyNewUser");

    given()
        .auth()
        .basic("headFirst", "headFirst")
        .contentType(ContentType.JSON)
        .body(createDTO)
        .when()
        .post("/api/employee/department-create")
        .then()
        .statusCode(201);
  }

  @Test
  public void whenCreateEmployeeAsAdmin_thenCreated() {
    EmployeeCreateDTO createDTO = createDummyEmployeeDTO();
    createDTO.setUsername("TotallyNewUser123");

    given()
        .auth()
        .basic("admin", "admin")
        .contentType(ContentType.JSON)
        .body(createDTO)
        .when()
        .post("/api/employee/admin-create")
        .then()
        .statusCode(201);
  }

  private EmployeeCreateDTO createDummyEmployeeDTO() {
    EmployeeCreateDTO employeeCreateDTO = new EmployeeCreateDTO();
    employeeCreateDTO.setAge(22);
    employeeCreateDTO.setName("NEW_VALID_USER");
    employeeCreateDTO.setUsername("NEW_VALID_USER");
    employeeCreateDTO.setSurname("NEW_VALID_USER");
    employeeCreateDTO.setPosition(PositionEnum.EMPLOYEE.name());
    employeeCreateDTO.setPersonalIdentificationNumber("123123");
    employeeCreateDTO.setDepartmentId(1L);
    return employeeCreateDTO;
  }
}
