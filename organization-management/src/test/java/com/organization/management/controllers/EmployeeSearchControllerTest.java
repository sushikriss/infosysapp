package com.organization.management.controllers;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class EmployeeSearchControllerTest {
  @LocalServerPort
  private int port;

  @Before
  public void setup() {
    RestAssured.baseURI = "http://localhost";
    RestAssured.port = port;
  }

  @Test
  public void whenGetProfile_thenOk() {
    given()
        .auth()
        .basic("employee4", "employee4")
        .when()
        .get("/api/employee/view/my-profile")
        .then()
        .statusCode(200)
        .body("name", equalTo("Bob"));
  }

  @Test
  public void whenGetEmployeesForDirector_thenOk() {
    given()
        .auth()
        .basic("directorOne", "directorOne")
        .when()
        .get("/api/employee/view/employees-in-directorate")
        .then()
        .statusCode(200)
        .body("size()", greaterThan(0));
  }

  @Test
  public void whenGetEmployeesForHeadOfDepartment_thenOk() {
    given()
        .auth()
        .basic("headFirst", "headFirst")
        .when()
        .get("/api/employee/view/employees-in-department")
        .then()
        .statusCode(200)
        .body("size()", greaterThan(0));
  }

  @Test
  public void whenGetAllPagedEmployeesForAdmin_thenOk() {
    given()
        .auth()
        .basic("admin", "admin")
        .when()
        .get("/api/employee/view/all/admin?page=0&size=10")
        .then()
        .statusCode(200)
        .body("size()", greaterThan(0));
  }

  @Test
  public void whenGetEmployeeForAdmin_thenOk() {
    given()
        .auth()
        .basic("admin", "admin")
        .when()
        .get("/api/employee/view/{id}/admin", 4L)
        .then()
        .statusCode(200)
        .body("name", equalTo("Bob"));
  }
}
