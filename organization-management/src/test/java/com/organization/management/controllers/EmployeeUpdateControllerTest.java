package com.organization.management.controllers;

import com.organization.management.web.dto.employee.EmployeeUpdateDTO;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class EmployeeUpdateControllerTest {
  @LocalServerPort
  private int port;

  @Before
  public void setup() {
    RestAssured.baseURI = "http://localhost";
    RestAssured.port = port;
  }

  @Test
  public void whenUpdateEmployeeAsDirector_thenOk() {
    EmployeeUpdateDTO updateDTO = createDummyEmployeeDTO();

    given()
        .auth()
        .basic("directorOne", "directorOne")
        .contentType(ContentType.JSON)
        .body(updateDTO)
        .when()
        .put("/api/employees/update/{id}/director", 4L)
        .then()
        .statusCode(200);
  }

  @Test
  public void whenDisableEmployeeAsDirector_thenOk() {
    given()
        .auth()
        .basic("directorOne", "directorOne")
        .when()
        .patch("/api/employees/update/{id}/director", 4L)
        .then()
        .statusCode(200);
  }

  @Test
  public void whenUpdateEmployeeAsHeadOfDepartment_thenOk() {
    EmployeeUpdateDTO updateDTO = createDummyEmployeeDTO();

    given()
        .auth()
        .basic("headFirst", "headFirst")
        .contentType(ContentType.JSON)
        .body(updateDTO)
        .when()
        .put("/api/employees/update/{id}/head", 4L)
        .then()
        .statusCode(200);
  }

  @Test
  public void whenDisableEmployeeAsHeadOfDepartment_thenOk() {
    given()
        .auth()
        .basic("headFirst", "headFirst")
        .when()
        .patch("/api/employees/update/{id}/head", 4L)
        .then()
        .statusCode(200);
  }

  @Test
  public void whenUpdateEmployeeAsAdmin_thenOk() {
    EmployeeUpdateDTO updateDTO = createDummyEmployeeDTO();

    given()
        .auth()
        .basic("admin", "admin")
        .contentType(ContentType.JSON)
        .body(updateDTO)
        .when()
        .put("/api/employees/update/{id}", 4L)
        .then()
        .statusCode(200);
  }

  @Test
  public void whenDisableEmployeeAsAdmin_thenOk() {
    given()
        .auth()
        .basic("admin", "admin")
        .when()
        .patch("/api/employees/update/{id}", 4L)
        .then()
        .statusCode(200);
  }

  private EmployeeUpdateDTO createDummyEmployeeDTO() {
    EmployeeUpdateDTO employeeUpdateDTO = new EmployeeUpdateDTO();
    employeeUpdateDTO.setAge(33);
    employeeUpdateDTO.setName("employee4");
    employeeUpdateDTO.setUsername("employee4");
    employeeUpdateDTO.setSurname("employee4");
    employeeUpdateDTO.setPersonalIdentificationNumber("123123");
    employeeUpdateDTO.setDepartmentId(1L);
    return employeeUpdateDTO;
  }
}
