package com.organization.management.service;

import com.organization.management.core.models.Employee;
import com.organization.management.core.repository.EmployeeRepository;
import com.organization.management.web.dto.employee.EmployeeCreateDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeCreationServiceTest {
  @Autowired
  private EmployeeCreationService employeeCreationService;

  @Test
  @Transactional
  public void givenAdmin_WhenCreateEmployee_ThenEmployeeIsCreated() {
    EmployeeCreateDTO employeeDTO = createTestEmployee();
    Employee employee = employeeCreationService.createEmployee(employeeDTO);
    assertNotNull(employee.getId());
  }

  @Test
  @Transactional
  public void givenDirectorName_WhenCreateEmployee_ThenEmployeeIsCreated() {
    String sqlDirectorName = "directorOne";
    EmployeeCreateDTO employeeDTO = createTestEmployee();
    Employee employee = employeeCreationService.createEmployeeAsDirector(employeeDTO, sqlDirectorName);
    assertNotNull(employee.getId());
  }

  @Test
  @Transactional
  public void givenHeadOfDepartmentName_WhenCreateEmployee_ThenEmployeeIsCreated() {
    String headOfDepartmentName = "headFirst";
    EmployeeCreateDTO employeeDTO = createTestEmployee();
    Employee employee = employeeCreationService.createEmployeeAsHeadOfDepartment(employeeDTO, headOfDepartmentName);
    assertNotNull(employee.getId());
  }

  private EmployeeCreateDTO createTestEmployee() {
    EmployeeCreateDTO employeeCreateDTO = new EmployeeCreateDTO();
    employeeCreateDTO.setName("ArnoldNev");
    employeeCreateDTO.setSurname("Nev");
    employeeCreateDTO.setUsername("arnold");
    employeeCreateDTO.setAge(40);
    employeeCreateDTO.setPosition("EMPLOYEE");
    employeeCreateDTO.setPersonalIdentificationNumber("123123123");
    employeeCreateDTO.setDepartmentId(1L);
    return employeeCreateDTO;
  }
}
