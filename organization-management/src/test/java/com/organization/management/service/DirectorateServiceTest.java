package com.organization.management.service;

import com.organization.management.core.models.Directorate;
import com.organization.management.web.dto.directorate.DirectorateFormDTO;
import com.organization.management.web.dto.employee.EmployeeDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DirectorateServiceTest {
  @Autowired
  private DirectorateService directorateService;

  @Test
  public void givenDirectorateId_WhenFindById_ThenReturnDirectorate() {
    Long directorateId = 1L;
    Directorate directorate = directorateService.findById(directorateId);
    assertNotNull(directorate);
  }

  @Test
  @Transactional
  public void givenDirectorateFormDTO_WhenCreateDirectorate_ThenDirectorateIsCreated() {
    DirectorateFormDTO directorateFormDTO = createTestDirectorateFormDTO();
    Directorate directorate = directorateService.createDirectorate(directorateFormDTO);
    assertNotNull(directorate);
    assertEquals(directorateFormDTO.getName(), directorate.getName());
  }

  @Test
  @Transactional
  public void givenDirectorateIdAndDirectorateFormDTO_WhenUpdateDirectorate_ThenDirectorateIsUpdated() {
    Long directorateId = 1L;
    DirectorateFormDTO directorateFormDTO = createTestDirectorateFormDTO();
    Directorate directorate = directorateService.updateDirectorate(directorateId, directorateFormDTO);
    assertNotNull(directorate);
    assertEquals(directorateFormDTO.getName(), directorate.getName());
  }

  private DirectorateFormDTO createTestDirectorateFormDTO() {
    DirectorateFormDTO directorateFormDTO = new DirectorateFormDTO();
    directorateFormDTO.setName("DirectorateName");
    directorateFormDTO.setDescription("DirectorateDescription");
    EmployeeDTO directorOfDirectorate = new EmployeeDTO();
    directorOfDirectorate.setUsername("admin");
    directorateFormDTO.setDirectorOfDirectorate(directorOfDirectorate);
    return directorateFormDTO;
  }
}
