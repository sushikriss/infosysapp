package com.organization.management.service;

import com.organization.management.core.models.Employee;
import com.organization.management.web.dto.employee.EmployeeUpdateDTO;
import jakarta.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeUpdateServiceTest {
  @Autowired
  private EmployeeUpdateService employeeUpdateService;

  @Test
  @Transactional
  public void givenDirectorNameAndEmployeeId_WhenUpdateEmployee_ThenEmployeeIsUpdated() {
    Long employeeId = 8L;
    String directorName = "directorOne";
    EmployeeUpdateDTO employeeUpdateDTO = createTestEmployeeUpdateDTO();
    Employee employee = employeeUpdateService.updateEmployeeAsDirector(employeeId, employeeUpdateDTO, directorName);
    assertNotNull(employee);
    assertEquals(employeeUpdateDTO.getName(), employee.getName());
  }

  @Test
  @Transactional
  public void givenHeadOfDepartmentNameAndEmployeeId_WhenUpdateEmployee_ThenEmployeeIsUpdated() {
    Long employeeId = 3L;
    String headOfDepartmentName = "headFirst";
    EmployeeUpdateDTO employeeUpdateDTO = createTestEmployeeUpdateDTO();
    Employee employee = employeeUpdateService.updateEmployeeAsHeadOfDepartment(employeeId, employeeUpdateDTO, headOfDepartmentName);
    assertNotNull(employee);
    assertEquals(employeeUpdateDTO.getName(), employee.getName());
  }

  @Test
  @Transactional
  public void givenEmployeeId_WhenUpdateEmployee_ThenEmployeeIsUpdated() {
    Long employeeId = 8L;
    EmployeeUpdateDTO employeeUpdateDTO = createTestEmployeeUpdateDTO();
    Employee employee = employeeUpdateService.updateEmployee(employeeId, employeeUpdateDTO);
    assertNotNull(employee);
    assertEquals(employeeUpdateDTO.getName(), employee.getName());
  }

  private EmployeeUpdateDTO createTestEmployeeUpdateDTO() {
    EmployeeUpdateDTO employeeUpdateDTO = new EmployeeUpdateDTO();
    employeeUpdateDTO.setName("ArnoldUpdated");
    employeeUpdateDTO.setSurname("TestUpdate");
    employeeUpdateDTO.setUsername("arnoldTestUsername");
    employeeUpdateDTO.setAge(30);
    employeeUpdateDTO.setPersonalIdentificationNumber("123123123");
    employeeUpdateDTO.setDepartmentId(1L);
    return employeeUpdateDTO;
  }
}
