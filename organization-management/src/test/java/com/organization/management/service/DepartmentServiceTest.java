package com.organization.management.service;

import com.organization.management.core.models.Department;
import com.organization.management.web.dto.department.DepartmentFormDTO;
import com.organization.management.web.dto.employee.EmployeeDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DepartmentServiceTest {
  @Autowired
  private DepartmentService departmentService;

  @Test
  public void givenDepartmentId_WhenFindById_ThenReturnDepartment() {
    Long departmentId = 1L;
    Department department = departmentService.findDepartmentById(departmentId);
    assertNotNull(department);
  }

  @Test
  @Transactional
  public void givenDepartmentFormDTO_WhenCreateDepartment_ThenDepartmentIsCreated() {
    DepartmentFormDTO departmentFormDTO = createTestDepartmentFormDTO();
    Department department = departmentService.createDepartment(departmentFormDTO);
    assertNotNull(department);
    assertEquals(departmentFormDTO.getName(), department.getName());
  }

  @Test
  @Transactional
  public void givenDepartmentIdAndDepartmentFormDTO_WhenUpdateDepartment_ThenDepartmentIsUpdated() {
    Long departmentId = 1L;
    DepartmentFormDTO departmentFormDTO = createTestDepartmentFormDTO();
    Department department = departmentService.updateDepartment(departmentId, departmentFormDTO);
    assertNotNull(department);
    assertEquals(departmentFormDTO.getName(), department.getName());
  }

  private DepartmentFormDTO createTestDepartmentFormDTO() {
    DepartmentFormDTO departmentFormDTO = new DepartmentFormDTO();
    departmentFormDTO.setName("DepartmentName");
    departmentFormDTO.setDescription("DepartmentDescription");
    EmployeeDTO headOfDepartment = new EmployeeDTO();
    headOfDepartment.setUsername("admin");
    departmentFormDTO.setHeadOfDepartment(headOfDepartment);
    return departmentFormDTO;
  }
}
