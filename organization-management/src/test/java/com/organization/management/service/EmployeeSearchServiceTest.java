package com.organization.management.service;

import com.organization.management.core.models.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeSearchServiceTest {
  @Autowired
  private EmployeeSearchService employeeSearchService;

  @Test
  public void givenDirectorName_WhenGetEmployees_ThenReturnEmployees() {
    String directorName = "directorOne";
    List<Employee> employees = employeeSearchService.getEmployeesForTheDirector(directorName);
    assertNotNull(employees);
    assertFalse(employees.isEmpty());
  }

  @Test
  public void givenHeadOfDepartmentName_WhenGetEmployees_ThenReturnEmployees() {
    String headOfDepartmentName = "headFirst";
    List<Employee> employees = employeeSearchService.getEmployeesForTheHeadOfDepartment(headOfDepartmentName);
    assertNotNull(employees);
    assertFalse(employees.isEmpty());
  }

  @Test
  public void whenGetAllEmployeesInPage_ThenReturnEmployees() {
    Pageable pageable = PageRequest.of(0, 5);
    List<Employee> employees = employeeSearchService.getAllEmployeesInPage(pageable);
    assertNotNull(employees);
    assertFalse(employees.isEmpty());
  }

  @Test
  public void givenEmployeeId_WhenGetEmployeeById_ThenReturnEmployee() {
    Long employeeId = 1L;
    Employee employee = employeeSearchService.getEmployeeById(employeeId);
    assertNotNull(employee);
  }

  @Test
  public void givenUsername_WhenFindByUsername_ThenReturnEmployee() {
    String username = "employee8";
    Employee employee = employeeSearchService.findByUsername(username);
    assertEquals(username, employee.getUsername());
  }
}
