package com.organization.management.core.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.organization.management.core.models.base.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "department")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Department extends BaseEntity {
  private String name;
  private String description;

  @OneToMany(mappedBy = "department")
  @JsonManagedReference
  private List<Employee> employees;

  @OneToOne
  private Employee headOfDepartment;

  @ManyToOne
  private Directorate directorate;
}
