package com.organization.management.core.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.organization.management.core.enums.PositionEnum;
import com.organization.management.core.models.base.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "employee")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Employee extends BaseEntity implements UserDetails {
  private String name;
  private String surname;
  private String username;
  private String personalIdentificationNumber;
  private Integer age;
  private boolean isEnabled = true;

  @Enumerated(EnumType.STRING)
  private PositionEnum position;

  @ManyToOne
  @JsonBackReference
  private Department department;

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null || getClass() != obj.getClass())
      return false;
    Employee employee = (Employee) obj;
    return Objects.equals(name, employee.name) &&
        Objects.equals(getId(), employee.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, getId());
  }

  @Override
  public String getPassword() {
    return personalIdentificationNumber;
  }

  @Override
  public boolean isEnabled() {
    return this.isEnabled;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return null;
  }

  @Override
  public boolean isAccountNonExpired() {
    return false;
  }

  @Override
  public boolean isAccountNonLocked() {
    return false;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return false;
  }
}
