package com.organization.management.core.repository;

import com.organization.management.core.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
  Optional<Employee> findByUsername(String username);

  @Query("SELECT e FROM Employee e WHERE e.department.directorate.id = :directorateId")
  List<Employee> findByDirectorateId(Long directorateId);

  @Query("SELECT e FROM Employee e WHERE e.department.id = :departmentId " +
      "AND (e.position = 'HEAD_OF_DEPARTMENT' OR e.position = 'EMPLOYEE')")
  List<Employee> findByDepartmentId(Long departmentId);

  @Query("SELECT e FROM Employee e WHERE e.id = :employeeId " +
      "AND e.department IN (SELECT d FROM Department d " +
      "WHERE d.directorate.directorOfDirectorate.username = :directorName)")
  Optional<Employee> findEmployeeByDirectorAndId(@Param("directorName") String directorName,
                                                 @Param("employeeId") Long employeeId);

  @Query("SELECT e FROM Employee e WHERE e.id = :employeeId " +
      "AND e.department.headOfDepartment.username = :headName " +
      "AND e.position != 'DIRECTOR'")
  Optional<Employee> findEmployeeByHeadAndId(@Param("headName") String headName,
                                             @Param("employeeId") Long employeeId);

  @Query("SELECT e FROM Employee e " +
      "WHERE e.department.directorate.directorOfDirectorate.username = :directorName")
  List<Employee> findEmployeesByDirector(@Param("directorName") String directorName);

  @Query("SELECT e FROM Employee e " +
      "WHERE e.department.headOfDepartment.username = :headName AND e.position != 'DIRECTOR'")
  List<Employee> findEmployeesByHeadOfDepartment(@Param("headName") String headName);
}
