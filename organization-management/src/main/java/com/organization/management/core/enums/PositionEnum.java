package com.organization.management.core.enums;

public enum PositionEnum {
  DIRECTOR_OF_DIRECTORATE,
  HEAD_OF_DEPARTMENT,
  EMPLOYEE,
  ADMIN
}
