package com.organization.management.core.models;

import com.organization.management.core.models.base.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "directorate")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Directorate extends BaseEntity {
  private String name;
  private String description;

  @OneToMany(mappedBy = "directorate")
  private List<Department> departments;

  @OneToOne
  private Employee directorOfDirectorate;
}
