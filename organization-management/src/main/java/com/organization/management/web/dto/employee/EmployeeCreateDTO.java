package com.organization.management.web.dto.employee;

import com.organization.management.web.validation.UniqueUsername;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeCreateDTO {
  @NotBlank(message = "Name cannot be blank")
  private String name;

  @NotBlank(message = "Surname cannot be blank")
  private String surname;

  @NotBlank(message = "Username cannot be blank")
  @UniqueUsername(message = "Username already exists")
  private String username;

  @NotBlank(message = "Personal Identification Number cannot be blank")
  private String personalIdentificationNumber;

  @Min(value = 18, message = "Age should not be less than 18")
  @Max(value = 65, message = "Age should not be greater than 65")
  private Integer age;

  @NotNull(message = "Position cannot be null")
  private String position;

  @NotNull(message = "Department cannot be null")
  private Long departmentId;
}
