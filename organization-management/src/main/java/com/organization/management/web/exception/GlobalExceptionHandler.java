package com.organization.management.web.exception;

import com.organization.management.web.exception.custom.DepartmentNotFoundException;
import com.organization.management.web.exception.custom.DirectorateNotFoundException;
import com.organization.management.web.exception.custom.EmployeeNotFoundException;
import com.organization.management.web.exception.custom.UnauthorizedActionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
  @ExceptionHandler(UsernameNotFoundException.class)
  public ResponseEntity<String> handleUsernameNotFound(UsernameNotFoundException ex) {
    log.error(ex.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found - " + ex.getMessage());
  }

  @ExceptionHandler(EmployeeNotFoundException.class)
  public ResponseEntity<String> handleEmployeeNotFound(EmployeeNotFoundException ex) {
    log.error(ex.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
  }

  @ExceptionHandler(DepartmentNotFoundException.class)
  public ResponseEntity<String> handleDepartmentNotFound(DepartmentNotFoundException ex) {
    log.error(ex.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
  }

  @ExceptionHandler(DirectorateNotFoundException.class)
  public ResponseEntity<String> handleDirectorateNotFoundException(DirectorateNotFoundException ex) {
    log.error(ex.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException ex) {
    log.error(ex.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
  }

  @ExceptionHandler(UnauthorizedActionException.class)
  public ResponseEntity<String> handleUnauthorizedActionException(UnauthorizedActionException ex) {
    log.error(ex.getMessage());
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
  }
}
