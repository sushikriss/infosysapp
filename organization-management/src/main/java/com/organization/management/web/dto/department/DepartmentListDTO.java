package com.organization.management.web.dto.department;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DepartmentListDTO {
  private List<DepartmentDTO> departments;
}
