package com.organization.management.web.dto.employee;

import com.organization.management.web.dto.department.DepartmentDTO;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmployeeDTO {
  private String name;
  private String surname;
  private String username;
  private Integer age;
  private String position;
  private String departmentName;
  private String departmentDescription;
}
