package com.organization.management.web.dto.department;

import com.organization.management.web.dto.employee.EmployeeDTO;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepartmentFormDTO {
  @NotBlank(message = "Name of department cannot be blank")
  private String name;
  @NotBlank(message = "Description cannot be blank")
  private String description;
  private EmployeeDTO headOfDepartment;
}
