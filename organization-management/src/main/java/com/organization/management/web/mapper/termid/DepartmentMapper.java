package com.organization.management.web.mapper.termid;

import com.organization.management.core.models.Department;
import com.organization.management.web.dto.department.DepartmentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DepartmentMapper {

  @Mapping(source = "name", target = "name")
  @Mapping(source = "description", target = "description")
  @Mapping(source = "headOfDepartment.name", target = "headOfDepartment")
  @Mapping(source = "directorate.name", target = "directorate")
  @Mapping(source = "employees", target = "employeesDTO")
  DepartmentDTO toDepartmentDTO(Department department);
}
