package com.organization.management.web.controllers;

import com.organization.management.facade.EmployeeFacade;
import com.organization.management.web.dto.employee.EmployeeDTO;
import com.organization.management.web.dto.employee.EmployeeUpdateDTO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/api/employees/update")
@Slf4j
public class EmployeeUpdateController {
  private final EmployeeFacade employeeFacade;

  public EmployeeUpdateController(EmployeeFacade employeeFacade) {
    this.employeeFacade = employeeFacade;
  }

  @PutMapping("/{id}/director")
  @PreAuthorize("hasRole('DIRECTOR_OF_DIRECTORATE')")
  public ResponseEntity<EmployeeDTO> updateEmployeeAsDirector(@PathVariable Long id,
                                                              @Valid @RequestBody EmployeeUpdateDTO updateDTO,
                                                              Principal principal) {

    EmployeeDTO updatedEmployee = employeeFacade.updateEmployeeAsDirector(id, updateDTO, principal.getName());
    log.info(principal.getName() + " Updated user: " + id);
    return ResponseEntity.ok(updatedEmployee);
  }

  @PatchMapping("/{id}/director")
  @PreAuthorize("hasRole('DIRECTOR_OF_DIRECTORATE')")
  public ResponseEntity<Void> disableEmployeeAsDirector(@PathVariable Long id, Principal principal) {

    employeeFacade.disableEmployeeAsDirector(id, principal.getName());
    log.info(principal.getName() + " Disabled user: " + id);
    return ResponseEntity.ok().build();
  }

  @PutMapping("/{id}/head")
  @PreAuthorize("hasRole('HEAD_OF_DEPARTMENT')")
  public ResponseEntity<EmployeeDTO> updateEmployeeAsHeadOfDepartment(
      @PathVariable Long id,
      @Valid @RequestBody EmployeeUpdateDTO updateDTO,
      Principal principal) {

    EmployeeDTO updatedEmployee = employeeFacade.
        updateEmployeeAsHeadOfDepartment(id, updateDTO, principal.getName());
    log.info(principal.getName() + " Updated user: " + id);
    return ResponseEntity.ok(updatedEmployee);
  }

  @PatchMapping("/{id}/head")
  @PreAuthorize("hasRole('HEAD_OF_DEPARTMENT')")
  public ResponseEntity<Void> disableEmployeeAsHeadOfDepartment(@PathVariable Long id,
                                                                Principal principal) {

    employeeFacade.disableEmployeeAsHeadOfDepartment(id, principal.getName());
    log.info(principal.getName() + " Disabled user: " + id);
    return ResponseEntity.ok().build();
  }

  @PutMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<EmployeeDTO> updateEmployeeAsAdmin(@PathVariable Long id,
                                                           @Valid @RequestBody EmployeeUpdateDTO updateDTO,
                                                           Principal principal) {
    EmployeeDTO updatedEmployee = employeeFacade.updateEmployee(id, updateDTO);
    log.info(principal.getName() + " Updated user: " + id);
    return ResponseEntity.ok(updatedEmployee);
  }

  @PatchMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<Void> disableEmployeeAsAdmin(@PathVariable Long id, Principal principal) {

    employeeFacade.disableEmployee(id);
    log.info(principal.getName() + " Disabled user with id: " + id);
    return ResponseEntity.ok().build();
  }
}
