package com.organization.management.web.mapper.termid;

import com.organization.management.core.models.Employee;
import com.organization.management.web.dto.employee.EmployeeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

  @Mapping(source = "name", target = "name")
  @Mapping(source = "surname", target = "surname")
  @Mapping(source = "username", target = "username")
  @Mapping(source = "age", target = "age")
  @Mapping(source = "position", target = "position")
  @Mapping(source = "department.name", target = "departmentName")
  @Mapping(source = "department.description", target = "departmentDescription")
  EmployeeDTO toEmployeeDTO(Employee employee);

  List<EmployeeDTO> toEmployeeDTOs(List<Employee> employees);
}
