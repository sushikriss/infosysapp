package com.organization.management.web.exception.custom;

public class DepartmentNotFoundException extends RuntimeException {
  public DepartmentNotFoundException(Long id) {
    super("Department with id " + id + " not found");
  }
}
