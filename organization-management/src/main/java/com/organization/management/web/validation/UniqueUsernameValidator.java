package com.organization.management.web.validation;

import com.organization.management.service.EmployeeSearchService;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {
  private final EmployeeSearchService employeeSearchService;

  public UniqueUsernameValidator(EmployeeSearchService employeeSearchService) {
    this.employeeSearchService = employeeSearchService;
  }

  @Override
  public void initialize(UniqueUsername constraintAnnotation) {
    ConstraintValidator.super.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(String username, ConstraintValidatorContext context) {
    try {
      return this.employeeSearchService.findByUsername(username) == null;
    } catch (UsernameNotFoundException ex) {
      return true;
    }
  }
}