package com.organization.management.web.controllers;

import com.organization.management.facade.DirectorateFacade;
import com.organization.management.web.dto.directorate.DirectorateFormDTO;
import com.organization.management.web.dto.directorate.DirectorateDTO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/api/directorate")
@Slf4j
public class DirectorateController {
  private final DirectorateFacade directorateFacade;

  public DirectorateController(DirectorateFacade directorateFacade) {
    this.directorateFacade = directorateFacade;
  }

  @GetMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN') || hasRole('DIRECTOR_OF_DIRECTORATE')")
  public ResponseEntity<DirectorateDTO> getDirectorate(@PathVariable Long id) {
    DirectorateDTO directorateDTO = directorateFacade.getDirectorate(id);
    return ResponseEntity.ok(directorateDTO);
  }

  @PostMapping
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<DirectorateDTO> createDirectorate(
      @Valid @RequestBody DirectorateFormDTO directorateFormDTO, Principal principal) {

    DirectorateDTO directorateDTO = directorateFacade.createDirectorate(directorateFormDTO);
    log.info(principal.getName() + " Created directorate: " + directorateDTO.getName());
    return ResponseEntity.ok(directorateDTO);
  }

  @PutMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<DirectorateDTO> updateDirectorate(
      @Valid @RequestBody DirectorateFormDTO directorateFormDTO,
      @PathVariable Long id, Principal principal) {
    DirectorateDTO directorateDTO = directorateFacade.updateDirectorate(id, directorateFormDTO);
    log.info(principal.getName() + " Updated directorate: " + directorateDTO.getName());
    return ResponseEntity.ok(directorateDTO);
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<Void> deleteDirectorate(@PathVariable Long id,
                                                Principal principal) {
    directorateFacade.deleteDirectorate(id);
    log.info(principal.getName() + " Deleted directorate with id: " + id);
    return ResponseEntity.ok().build();
  }
}
