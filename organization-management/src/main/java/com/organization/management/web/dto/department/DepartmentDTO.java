package com.organization.management.web.dto.department;

import com.organization.management.web.dto.directorate.DirectorateDTO;
import com.organization.management.web.dto.employee.EmployeeDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DepartmentDTO {
  private String name;
  private String description;
  private List<EmployeeDTO> employeesDTO;
  private String headOfDepartment;
  private String directorate;
}
