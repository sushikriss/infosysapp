package com.organization.management.web.dto.directorate;

import com.organization.management.web.dto.department.DepartmentDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DirectorateDTO {
  private String name;
  private String description;
  private String directorOfDirectorate;
  private List<DepartmentDTO> departmentDTOs;
}
