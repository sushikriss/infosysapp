package com.organization.management.web.exception.custom;

public class EmployeeNotFoundException extends RuntimeException {
  public EmployeeNotFoundException(Long id) {
    super("Employee with id " + id + " not found");
  }
}
