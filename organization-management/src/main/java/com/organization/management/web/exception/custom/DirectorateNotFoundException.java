package com.organization.management.web.exception.custom;

public class DirectorateNotFoundException extends RuntimeException {
  public DirectorateNotFoundException(Long id) {
    super("Directorate with id " + id + " not found");
  }
}

