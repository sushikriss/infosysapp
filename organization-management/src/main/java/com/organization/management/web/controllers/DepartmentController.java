package com.organization.management.web.controllers;

import com.organization.management.facade.DepartmentFacade;
import com.organization.management.web.dto.department.DepartmentDTO;
import com.organization.management.web.dto.department.DepartmentFormDTO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/api/department")
@Slf4j
public class DepartmentController {
  private final DepartmentFacade departmentFacade;

  public DepartmentController(DepartmentFacade departmentFacade) {
    this.departmentFacade = departmentFacade;
  }

  @GetMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN') || hasRole('HEAD_OF_DEPARTMENT') || hasRole('DIRECTOR_OF_DIRECTORATE')")
  public ResponseEntity<DepartmentDTO> getDepartment(@PathVariable Long id) {
    DepartmentDTO departmentDTO = departmentFacade.getDepartment(id);
    return ResponseEntity.ok(departmentDTO);
  }

  @PostMapping
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<DepartmentDTO> createDepartment(
      @Valid @RequestBody DepartmentFormDTO departmentFormDTO, Principal principal) {
    DepartmentDTO departmentDTO = departmentFacade.createDepartment(departmentFormDTO);
    log.info(principal.getName() + " Created department: " + departmentDTO.getName());
    return ResponseEntity.ok(departmentDTO);
  }

  @PutMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<DepartmentDTO> updateDepartment(
      @Valid @RequestBody DepartmentFormDTO departmentFormDTO,
      @PathVariable Long id, Principal principal) {
    DepartmentDTO departmentDTO = departmentFacade.updateDepartment(id, departmentFormDTO);
    log.info(principal.getName() + " Updated department: " + departmentDTO.getName());
    return ResponseEntity.ok(departmentDTO);
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<Void> deleteDepartment(@PathVariable Long id,
                                               Principal principal) {
    departmentFacade.deleteDepartment(id);
    log.info(principal.getName() + " Deleted department with id: " + id);
    return ResponseEntity.ok().build();
  }
}
