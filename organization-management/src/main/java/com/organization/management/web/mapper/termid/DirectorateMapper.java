package com.organization.management.web.mapper.termid;

import com.organization.management.core.models.Directorate;
import com.organization.management.web.dto.directorate.DirectorateDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = DepartmentMapper.class)
public interface DirectorateMapper {

  @Mapping(source = "name", target = "name")
  @Mapping(source = "description", target = "description")
  @Mapping(source = "directorOfDirectorate.name", target = "directorOfDirectorate")
  @Mapping(source = "departments", target = "departmentDTOs")
  DirectorateDTO toDirectorateDTO(Directorate directorate);

  List<DirectorateDTO> toDirectorateDTOs(List<Directorate> directorates);
}
