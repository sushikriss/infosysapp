package com.organization.management.web.controllers;

import com.organization.management.facade.EmployeeFacade;
import com.organization.management.web.dto.employee.EmployeeCreateDTO;
import com.organization.management.web.dto.employee.EmployeeDTO;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/api/employee")
@Slf4j
public class EmployeeCreationController {
  private final EmployeeFacade employeeFacade;

  public EmployeeCreationController(EmployeeFacade employeeFacade) {
    this.employeeFacade = employeeFacade;
  }

  @PostMapping("/director-create")
  @PreAuthorize("hasRole('DIRECTOR_OF_DIRECTORATE')")
  public ResponseEntity<EmployeeDTO> createEmployeeAsDirector(@Valid @RequestBody EmployeeCreateDTO createDTO,
                                                              Principal principal) {

    EmployeeDTO newEmployee = employeeFacade.createEmployeeAsDirector(createDTO, principal.getName());
    log.info(principal.getName() + " Created user: " + newEmployee.getName());
    return ResponseEntity.status(HttpStatus.CREATED).body(newEmployee);
  }

  @PostMapping("/department-create")
  @PreAuthorize("hasRole('ROLE_HEAD_OF_DEPARTMENT')")
  public ResponseEntity<EmployeeDTO> createEmployeeAsHead(@Valid @RequestBody EmployeeCreateDTO createDTO,
                                                          Principal principal) {

    EmployeeDTO newEmployee = employeeFacade.createEmployeeAsHeadOfDepartment(createDTO, principal.getName());
    log.info(principal.getName() + " Created user: " + newEmployee.getName());
    return ResponseEntity.status(HttpStatus.CREATED).body(newEmployee);
  }

  @PostMapping("/admin-create")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<EmployeeDTO> createEmployeeAsAdmin(@Valid @RequestBody EmployeeCreateDTO createDTO,
                                                           Principal principal) {

    EmployeeDTO newEmployee = employeeFacade.createEmployee(createDTO);
    log.info(principal.getName() + " Created user: " + newEmployee.getName());
    return ResponseEntity.status(HttpStatus.CREATED).body(newEmployee);
  }
}
