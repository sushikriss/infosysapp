package com.organization.management.web.controllers;

import com.organization.management.facade.EmployeeFacade;
import com.organization.management.web.dto.employee.EmployeeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/api/employee/view")
@Slf4j
public class EmployeeSearchController {
  private final EmployeeFacade employeeFacade;

  public EmployeeSearchController(EmployeeFacade employeeFacade) {
    this.employeeFacade = employeeFacade;
  }

  @GetMapping("/my-profile")
  public ResponseEntity<EmployeeDTO> getProfile(Principal principal) {
    EmployeeDTO employee = employeeFacade.getEmployeeProfile(principal.getName());
    return ResponseEntity.ok(employee);
  }

  @GetMapping("/employees-in-directorate")
  @PreAuthorize("hasRole('ROLE_DIRECTOR_OF_DIRECTORATE')")
  public ResponseEntity<List<EmployeeDTO>> getEmployeesForDirector(Principal principal) {

    List<EmployeeDTO> employeeListDTO = employeeFacade.getEmployeesForDirector(principal.getName());
    return ResponseEntity.ok(employeeListDTO);
  }

  @GetMapping("/employees-in-department")
  @PreAuthorize("hasRole('ROLE_HEAD_OF_DEPARTMENT')")
  public ResponseEntity<List<EmployeeDTO>> getEmployeesForHeadOfDepartment(Principal principal) {

    List<EmployeeDTO> employeeListDTO = employeeFacade.getEmployeesForHeadOfDepartment(principal.getName());
    return ResponseEntity.ok(employeeListDTO);
  }

  @GetMapping("/all/admin")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<List<EmployeeDTO>> getAllEmployeesForAdmin(@RequestParam(defaultValue = "0") int page,
                                                                 @RequestParam(defaultValue = "10") int size) {
    Pageable pageable = PageRequest.of(page, size);
    List<EmployeeDTO> employeeListDTO = employeeFacade.getAllEmployees(pageable);
    return ResponseEntity.ok(employeeListDTO);
  }

  @GetMapping("/{id}/admin")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<EmployeeDTO> getEmployeeForAdmin(@PathVariable Long id) {
    EmployeeDTO employee = employeeFacade.getEmployee(id);
    return ResponseEntity.ok(employee);
  }
}
