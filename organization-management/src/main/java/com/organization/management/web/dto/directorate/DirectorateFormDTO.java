package com.organization.management.web.dto.directorate;

import com.organization.management.web.dto.employee.EmployeeDTO;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DirectorateFormDTO {
  @NotBlank(message = "Name of directorate cannot be blank")
  private String name;
  @NotBlank(message = "Description cannot be blank")
  private String description;
  private EmployeeDTO directorOfDirectorate;
}
