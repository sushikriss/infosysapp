package com.organization.management.facade;

import com.organization.management.web.dto.employee.EmployeeCreateDTO;
import com.organization.management.web.dto.employee.EmployeeDTO;
import com.organization.management.web.dto.employee.EmployeeUpdateDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EmployeeFacade {
  EmployeeDTO createEmployeeAsDirector(EmployeeCreateDTO createDTO, String directorName);

  EmployeeDTO createEmployeeAsHeadOfDepartment(EmployeeCreateDTO createDTO, String name);

  EmployeeDTO createEmployee(EmployeeCreateDTO createDTO);

  EmployeeDTO getEmployeeProfile(String name);

  EmployeeDTO getEmployee(Long id);

  List<EmployeeDTO> getAllEmployees(Pageable pageable);

  List<EmployeeDTO> getEmployeesForHeadOfDepartment(String headOfDepartmentName);

  List<EmployeeDTO> getEmployeesForDirector(String name);

  EmployeeDTO updateEmployee(Long id, EmployeeUpdateDTO updateDTO);

  void disableEmployee(Long id);

  EmployeeDTO updateEmployeeAsDirector(Long id, EmployeeUpdateDTO updateDTO, String name);

  void disableEmployeeAsDirector(Long id, String name);

  EmployeeDTO updateEmployeeAsHeadOfDepartment(Long id, EmployeeUpdateDTO updateDTO, String name);

  void disableEmployeeAsHeadOfDepartment(Long id, String name);
}
