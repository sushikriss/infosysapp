package com.organization.management.facade.impl;

import com.organization.management.core.models.Directorate;
import com.organization.management.facade.DirectorateFacade;
import com.organization.management.service.DirectorateService;
import com.organization.management.web.dto.directorate.DirectorateFormDTO;
import com.organization.management.web.dto.directorate.DirectorateDTO;
import com.organization.management.web.mapper.termid.DirectorateMapper;
import org.springframework.stereotype.Component;

@Component
public class DirectorateFacadeImpl implements DirectorateFacade {
  private final DirectorateService directorateService;
  private final DirectorateMapper directorateMapper;

  public DirectorateFacadeImpl(DirectorateService directorateService, DirectorateMapper directorateMapper) {
    this.directorateService = directorateService;
    this.directorateMapper = directorateMapper;
  }

  @Override
  public DirectorateDTO getDirectorate(Long id) {
    Directorate directorate = directorateService.findById(id);
    return directorateMapper.toDirectorateDTO(directorate);
  }

  @Override
  public DirectorateDTO updateDirectorate(Long id, DirectorateFormDTO DirectorateFormDTO) {
    Directorate directorate = directorateService.updateDirectorate(id, DirectorateFormDTO);
    return directorateMapper.toDirectorateDTO(directorate);
  }

  @Override
  public DirectorateDTO createDirectorate(DirectorateFormDTO directorateFormDTO) {
    Directorate directorate = directorateService.createDirectorate(directorateFormDTO);
    return directorateMapper.toDirectorateDTO(directorate);
  }

  @Override
  public void deleteDirectorate(Long id) {
   directorateService.deleteDirectorate(id);
  }
}
