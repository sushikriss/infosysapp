package com.organization.management.facade;

import com.organization.management.web.dto.directorate.DirectorateFormDTO;
import com.organization.management.web.dto.directorate.DirectorateDTO;

public interface DirectorateFacade {
  DirectorateDTO getDirectorate(Long id);

  DirectorateDTO createDirectorate(DirectorateFormDTO directorateFormDTO);

  DirectorateDTO updateDirectorate(Long id, DirectorateFormDTO directorateFormDTO);

  void deleteDirectorate(Long id);
}
