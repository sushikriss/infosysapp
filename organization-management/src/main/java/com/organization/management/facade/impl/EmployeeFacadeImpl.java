package com.organization.management.facade.impl;

import com.organization.management.core.models.Employee;
import com.organization.management.facade.EmployeeFacade;
import com.organization.management.service.EmployeeCreationService;
import com.organization.management.service.EmployeeSearchService;
import com.organization.management.service.EmployeeUpdateService;
import com.organization.management.web.dto.employee.EmployeeCreateDTO;
import com.organization.management.web.dto.employee.EmployeeDTO;
import com.organization.management.web.dto.employee.EmployeeUpdateDTO;
import com.organization.management.web.mapper.termid.EmployeeMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmployeeFacadeImpl implements EmployeeFacade {
  private final EmployeeCreationService employeeCreationService;
  private final EmployeeSearchService employeeSearchService;
  private final EmployeeUpdateService employeeUpdateService;
  private final EmployeeMapper employeeMapper;

  public EmployeeFacadeImpl(EmployeeCreationService employeeCreationService,
                            EmployeeSearchService employeeSearchService,
                            EmployeeUpdateService employeeUpdateService,
                            EmployeeMapper employeeMapper) {
    this.employeeCreationService = employeeCreationService;
    this.employeeSearchService = employeeSearchService;
    this.employeeUpdateService = employeeUpdateService;
    this.employeeMapper = employeeMapper;
  }

  @Override
  public EmployeeDTO createEmployeeAsDirector(EmployeeCreateDTO createDTO, String directorName) {
    Employee employee = employeeCreationService.createEmployeeAsDirector(createDTO, directorName);
    return employeeMapper.toEmployeeDTO(employee);
  }

  @Override
  public EmployeeDTO createEmployeeAsHeadOfDepartment(EmployeeCreateDTO createDTO, String username) {
    Employee employee = employeeCreationService.createEmployeeAsHeadOfDepartment(createDTO, username);
    return employeeMapper.toEmployeeDTO(employee);
  }

  @Override
  public EmployeeDTO createEmployee(EmployeeCreateDTO createDTO) {
    Employee employee = employeeCreationService.createEmployee(createDTO);
    return employeeMapper.toEmployeeDTO(employee);
  }

  @Override
  public EmployeeDTO getEmployeeProfile(String name) {
    Employee employee = employeeSearchService.findByUsername(name);
    return employeeMapper.toEmployeeDTO(employee);
  }

  @Override
  public EmployeeDTO getEmployee(Long id) {
    Employee employee = employeeSearchService.getEmployeeById(id);
    return employeeMapper.toEmployeeDTO(employee);
  }

  @Override
  public List<EmployeeDTO> getAllEmployees(Pageable pageable) {
    List<Employee> employees = employeeSearchService.getAllEmployeesInPage(pageable);
    return employeeMapper.toEmployeeDTOs(employees);
  }

  @Override
  public List<EmployeeDTO> getEmployeesForHeadOfDepartment(String headOfDepartmentName) {
    List<Employee> employees = employeeSearchService.getEmployeesForTheHeadOfDepartment(headOfDepartmentName);
    return employeeMapper.toEmployeeDTOs(employees);
  }

  @Override
  public List<EmployeeDTO> getEmployeesForDirector(String name) {
    List<Employee> employees = employeeSearchService.getEmployeesForTheDirector(name);
    return employeeMapper.toEmployeeDTOs(employees);
  }

  @Override
  public EmployeeDTO updateEmployee(Long id, EmployeeUpdateDTO updateDTO) {
    Employee employee = employeeUpdateService.updateEmployee(id, updateDTO);
    return employeeMapper.toEmployeeDTO(employee);
  }

  @Override
  public void disableEmployee(Long id) {
    employeeUpdateService.disableEmployee(id);
  }

  @Override
  public EmployeeDTO updateEmployeeAsDirector(Long employeeId,
                                              EmployeeUpdateDTO employeeUpdateDTO,
                                              String directorName) {
    Employee employee = employeeUpdateService
        .updateEmployeeAsDirector(employeeId, employeeUpdateDTO, directorName);
    return employeeMapper.toEmployeeDTO(employee);
  }

  @Override
  public void disableEmployeeAsDirector(Long id, String directorName) {
    employeeUpdateService.disableEmployeeAsDirector(id, directorName);
  }

  @Override
  public EmployeeDTO updateEmployeeAsHeadOfDepartment(Long id, EmployeeUpdateDTO employeeUpdateDTO, String headName) {
    Employee employee = employeeUpdateService.updateEmployeeAsHeadOfDepartment(id, employeeUpdateDTO, headName);
    return employeeMapper.toEmployeeDTO(employee);
  }

  @Override
  public void disableEmployeeAsHeadOfDepartment(Long id, String directorName) {
    employeeUpdateService.disableEmployeeAsHeadOfDepartment(id, directorName);
  }
}
