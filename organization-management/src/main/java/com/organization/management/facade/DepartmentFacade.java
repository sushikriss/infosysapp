package com.organization.management.facade;

import com.organization.management.web.dto.department.DepartmentDTO;
import com.organization.management.web.dto.department.DepartmentFormDTO;

public interface DepartmentFacade {
  DepartmentDTO getDepartment(Long id);

  DepartmentDTO createDepartment(DepartmentFormDTO departmentFormDTO);

  DepartmentDTO updateDepartment(Long id, DepartmentFormDTO departmentFormDTO);

  void deleteDepartment(Long id);
}
