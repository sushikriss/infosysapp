package com.organization.management.facade.impl;

import com.organization.management.core.models.Department;
import com.organization.management.facade.DepartmentFacade;
import com.organization.management.service.DepartmentService;
import com.organization.management.web.dto.department.DepartmentDTO;
import com.organization.management.web.dto.department.DepartmentFormDTO;
import com.organization.management.web.mapper.termid.DepartmentMapper;
import org.springframework.stereotype.Component;

@Component
public class DepartmentFacadeImpl implements DepartmentFacade {
  private final DepartmentService departmentService;
  private final DepartmentMapper departmentMapper;

  public DepartmentFacadeImpl(DepartmentService departmentService, DepartmentMapper departmentMapper) {
    this.departmentService = departmentService;
    this.departmentMapper = departmentMapper;
  }

  @Override
  public DepartmentDTO getDepartment(Long id) {
    Department department = departmentService.findDepartmentById(id);
    return departmentMapper.toDepartmentDTO(department);
  }

  @Override
  public DepartmentDTO createDepartment(DepartmentFormDTO departmentFormDTO) {
    Department department = departmentService.createDepartment(departmentFormDTO);
    return departmentMapper.toDepartmentDTO(department);
  }

  @Override
  public DepartmentDTO updateDepartment(Long id, DepartmentFormDTO departmentFormDTO) {
    Department department = departmentService.updateDepartment(id, departmentFormDTO);
    return departmentMapper.toDepartmentDTO(department);
  }

  @Override
  public void deleteDepartment(Long id) {
    departmentService.deleteDepartment(id);
  }
}
