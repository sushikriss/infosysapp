package com.organization.management.service;

import com.organization.management.core.models.Department;
import com.organization.management.core.models.Employee;
import com.organization.management.web.dto.department.DepartmentFormDTO;

public interface DepartmentService {
  Department findDepartmentById(Long departmentId);

  Department createDepartment(DepartmentFormDTO departmentFormDTO);

  Department updateDepartment(Long departmentId, DepartmentFormDTO departmentFormDTO);

  void replaceHeadOfDepartment(Employee employee, Department department);

  void deleteDepartment(Long departmentId);
}
