package com.organization.management.service;

import com.organization.management.core.models.Employee;
import com.organization.management.web.dto.employee.EmployeeUpdateDTO;

public interface EmployeeUpdateService {
  Employee updateEmployeeAsDirector(Long employeeId,
                                    EmployeeUpdateDTO employeeUpdateDTO,
                                    String directorName);

  Employee updateEmployeeAsHeadOfDepartment(Long employeeId,
                                            EmployeeUpdateDTO employeeUpdateDTO,
                                            String headOfDepartmentName);

  Employee updateEmployee(Long employeeId, EmployeeUpdateDTO updateDTO);

  void disableEmployeeAsDirector(Long employeeId, String directorName);

  void disableEmployeeAsHeadOfDepartment(Long employeeId, String headOfDepartmentName);

  void disableEmployee(Long employeeId);
}
