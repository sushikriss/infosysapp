package com.organization.management.service.impl;

import com.organization.management.core.models.Department;
import com.organization.management.core.models.Employee;
import com.organization.management.core.repository.DepartmentRepository;
import com.organization.management.service.DepartmentService;
import com.organization.management.web.dto.department.DepartmentFormDTO;
import com.organization.management.web.exception.custom.DepartmentNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImpl implements DepartmentService {
  private final DepartmentRepository departmentRepository;
  private final EmployeeSharedServiceImpl employeeSharedService;

  public DepartmentServiceImpl(DepartmentRepository departmentRepository,
                               EmployeeSharedServiceImpl employeeSharedService) {
    this.departmentRepository = departmentRepository;
    this.employeeSharedService = employeeSharedService;
  }

  @Override
  public void replaceHeadOfDepartment(Employee employee, Department department) {
    department.setHeadOfDepartment(employee);
    departmentRepository.save(department);
  }

  @Override
  public Department createDepartment(DepartmentFormDTO departmentFormDTO) {
    Employee futureHeadOfDepartment = findHeadOfDepartmentFromDTO(departmentFormDTO);

    Department department = buildDepartment(futureHeadOfDepartment, departmentFormDTO);
    departmentRepository.save(department);
    return department;
  }

  @Override
  public Department updateDepartment(Long departmentId, DepartmentFormDTO departmentFormDTO) {
    Department department = findDepartmentById(departmentId);
    Employee futureHeadOfDepartment = findHeadOfDepartmentFromDTO(departmentFormDTO);

    Department updatedDepartment = applyUpdatesToDepartment(futureHeadOfDepartment, departmentFormDTO, department);
    departmentRepository.save(updatedDepartment);
    return updatedDepartment;
  }

  @Override
  public void deleteDepartment(Long departmentId) {
    departmentRepository.deleteById(departmentId);
  }

  @Override
  public Department findDepartmentById(Long departmentId) {
    return departmentRepository.findById(departmentId)
        .orElseThrow(() -> new DepartmentNotFoundException(departmentId));
  }

  private Department applyUpdatesToDepartment(Employee futureHeadOfDepartment,
                                              DepartmentFormDTO departmentFormDTO,
                                              Department department) {
    department.setHeadOfDepartment(futureHeadOfDepartment);
    department.setName(departmentFormDTO.getName());
    department.setDescription(departmentFormDTO.getDescription());
    return department;
  }

  private Employee findHeadOfDepartmentFromDTO(DepartmentFormDTO departmentFormDTO) {
    String employeeName = departmentFormDTO.getHeadOfDepartment().getUsername();
    return employeeSharedService.findByUsername(employeeName);
  }

  private Department buildDepartment(Employee toBeHeadOfDepartment, DepartmentFormDTO departmentFormDTO) {
    Department department = new Department();
    department.setHeadOfDepartment(toBeHeadOfDepartment);
    department.setName(departmentFormDTO.getName());
    department.setDescription(departmentFormDTO.getDescription());
    return department;
  }
}
