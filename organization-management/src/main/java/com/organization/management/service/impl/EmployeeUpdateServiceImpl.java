package com.organization.management.service.impl;

import com.organization.management.core.models.Department;
import com.organization.management.core.models.Employee;
import com.organization.management.core.repository.EmployeeRepository;
import com.organization.management.service.DepartmentService;
import com.organization.management.service.EmployeeUpdateService;
import com.organization.management.web.dto.employee.EmployeeUpdateDTO;
import org.springframework.stereotype.Service;

@Service
public class EmployeeUpdateServiceImpl implements EmployeeUpdateService {
  private final EmployeeRepository employeeRepository;
  private final EmployeeSharedServiceImpl employeeSharedService;
  private final DepartmentService departmentService;

  public EmployeeUpdateServiceImpl(EmployeeRepository employeeRepository,
                                   EmployeeSharedServiceImpl employeeSharedService,
                                   DepartmentService departmentService) {
    this.employeeRepository = employeeRepository;
    this.employeeSharedService = employeeSharedService;
    this.departmentService = departmentService;
  }

  @Override
  public Employee updateEmployeeAsDirector(Long employeeId, EmployeeUpdateDTO employeeUpdateDTO,
                                           String directorName) {
    Employee employee = employeeSharedService.findEmployeeByDirectorAndId(employeeId, directorName);
    updateEmployeeModel(employee, employeeUpdateDTO);
    return employee;
  }

  @Override
  public void disableEmployeeAsDirector(Long employeeId, String directorName) {
    Employee employee = employeeSharedService.findEmployeeByDirectorAndId(employeeId, directorName);
    disableEmployee(employee);
  }

  @Override
  public Employee updateEmployeeAsHeadOfDepartment(Long employeeId, EmployeeUpdateDTO employeeUpdateDTO,
                                                   String headOfDepartmentName) {
    Employee employee = employeeSharedService.findEmployeeByHeadAndId(headOfDepartmentName, employeeId);
    updateEmployeeModel(employee, employeeUpdateDTO);
    return employee;
  }

  @Override
  public void disableEmployeeAsHeadOfDepartment(Long employeeId, String headOfDepartmentName) {
    Employee employee = employeeSharedService.findEmployeeByHeadAndId(headOfDepartmentName, employeeId);
    disableEmployee(employee);
  }

  @Override
  public Employee updateEmployee(Long employeeId, EmployeeUpdateDTO updateDTO) {
    Employee employee = employeeSharedService.findById(employeeId);
    updateEmployeeModel(employee, updateDTO);
    employeeRepository.save(employee);
    return employee;
  }

  @Override
  public void disableEmployee(Long employeeId) {
    Employee employee = employeeSharedService.findById(employeeId);
    disableEmployee(employee);
  }

  private void updateEmployeeModel(Employee employee, EmployeeUpdateDTO updateDTO) {
    checkUsernameUniqueness(employee, updateDTO.getUsername());
    Department department = departmentService.findDepartmentById(updateDTO.getDepartmentId());

    employee.setName(updateDTO.getName());
    employee.setSurname(updateDTO.getSurname());
    employee.setUsername(updateDTO.getUsername());
    employee.setAge(updateDTO.getAge());
    employee.setDepartment(department);
    employee.setPersonalIdentificationNumber(
        updateDTO.getPersonalIdentificationNumber());
  }

  private void checkUsernameUniqueness(Employee employee, String newUsername) {
    if (!employee.getUsername().equals(newUsername)) {
      Employee existingEmployee = employeeRepository.findByUsername(newUsername).orElse(null);
      if (existingEmployee != null && !existingEmployee.getId().equals(employee.getId())) {
        throw new IllegalArgumentException("Username already exists");
      }
    }
  }

  private void disableEmployee(Employee employee) {
    employee.setEnabled(false);
    employeeRepository.save(employee);
  }
}
