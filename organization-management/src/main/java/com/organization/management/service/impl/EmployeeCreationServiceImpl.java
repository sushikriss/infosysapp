package com.organization.management.service.impl;

import com.organization.management.core.enums.PositionEnum;
import com.organization.management.core.models.Department;
import com.organization.management.core.models.Employee;
import com.organization.management.core.repository.EmployeeRepository;
import com.organization.management.service.DepartmentService;
import com.organization.management.service.EmployeeCreationService;
import com.organization.management.web.dto.employee.EmployeeCreateDTO;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class EmployeeCreationServiceImpl implements EmployeeCreationService {
  private final EmployeeRepository employeeRepository;
  private final EmployeeSharedServiceImpl employeeSharedService;
  private final DepartmentService departmentService;
  private final PasswordEncoder passwordEncoder;

  public EmployeeCreationServiceImpl(EmployeeRepository employeeRepository,
                                     EmployeeSharedServiceImpl employeeSharedService,
                                     DepartmentService departmentService,
                                     PasswordEncoder passwordEncoder) {
    this.employeeRepository = employeeRepository;
    this.employeeSharedService = employeeSharedService;
    this.departmentService = departmentService;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public Employee createEmployeeAsDirector(EmployeeCreateDTO employeeDTO, String directorName) {
    employeeSharedService.validateIfNewEmployeeIsNotDirector(employeeDTO.getPosition());

    Employee director = employeeSharedService.findByUsername(directorName);
    Department department = departmentService.findDepartmentById(employeeDTO.getDepartmentId());
    employeeSharedService.validateDirectorOfDepartment(department, director);

    Employee employee = buildEmployee(employeeDTO);
    employeeRepository.save(employee);
    if (isEmployeeTheNewHeadOfDepartment(employee.getPosition())) {
      replaceCurrentHeadOfDepartment(employee, department);
    }
    return employee;
  }

  @Override
  public Employee createEmployeeAsHeadOfDepartment(EmployeeCreateDTO employeeDTO, String headOfDepartmentName) {
    employeeSharedService.validateIfNewEmployeeIsNotDirector(employeeDTO.getPosition());
    employeeSharedService.validateIfNewEmployeeIsNotHeadOfDepartment(employeeDTO.getPosition());

    Employee headOfDepartment = employeeSharedService.findByUsername(headOfDepartmentName);
    Department employeeDepartment = departmentService.findDepartmentById(employeeDTO.getDepartmentId());
    employeeSharedService.validateHeadOfDepartment(headOfDepartment, employeeDepartment);

    Employee employee = buildEmployee(employeeDTO);
    employeeRepository.save(employee);
    return employee;
  }

  @Override
  public Employee createEmployee(EmployeeCreateDTO createDTO) {
    Employee employee = buildEmployee(createDTO);
    employeeRepository.save(employee);
    return employee;
  }

  private Employee buildEmployee(EmployeeCreateDTO createDTO) {
    Employee employee = new Employee();
    Department department = departmentService.findDepartmentById(createDTO.getDepartmentId());
    PositionEnum position = PositionEnum.valueOf(createDTO.getPosition());

    employee.setName(createDTO.getName());
    employee.setSurname(createDTO.getSurname());
    employee.setUsername(createDTO.getUsername());
    employee.setAge(createDTO.getAge());
    employee.setDepartment(department);
    employee.setPosition(position);

    String pin = createDTO.getPersonalIdentificationNumber();
    employee.setPersonalIdentificationNumber(passwordEncoder.encode(pin));
    return employee;
  }

  private void replaceCurrentHeadOfDepartment(Employee employee, Department department) {
    Employee pastHeadOfDepartment = demoteCurrentHeadOfDepartment(department);
    employeeRepository.save(pastHeadOfDepartment);

    departmentService.replaceHeadOfDepartment(employee, department);
  }

  private Employee demoteCurrentHeadOfDepartment(Department department) {
    Employee currentHeadOfDepartment = department.getHeadOfDepartment();
    currentHeadOfDepartment.setPosition(PositionEnum.EMPLOYEE);
    return currentHeadOfDepartment;
  }

  private boolean isEmployeeTheNewHeadOfDepartment(PositionEnum position) {
    return PositionEnum.HEAD_OF_DEPARTMENT.equals(position);
  }
}
