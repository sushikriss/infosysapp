package com.organization.management.service;

import com.organization.management.core.models.Employee;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EmployeeSearchService {
  List<Employee> getEmployeesForTheDirector(String directorName);

  List<Employee> getEmployeesForTheHeadOfDepartment(String headOfDepartmentName);

  List<Employee> getAllEmployeesInPage(Pageable pageable);

  Employee findByUsername(String employeeUsername);

  Employee getEmployeeById(Long employeeId);
}
