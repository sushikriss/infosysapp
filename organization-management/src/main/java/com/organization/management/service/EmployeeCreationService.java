package com.organization.management.service;

import com.organization.management.core.models.Employee;
import com.organization.management.web.dto.employee.EmployeeCreateDTO;

public interface EmployeeCreationService {
  Employee createEmployeeAsDirector(EmployeeCreateDTO employeeDTO, String directorName);

  Employee createEmployeeAsHeadOfDepartment(EmployeeCreateDTO createDTO,
                                            String headOfDepartmentName);

  Employee createEmployee(EmployeeCreateDTO createDTO);
}
