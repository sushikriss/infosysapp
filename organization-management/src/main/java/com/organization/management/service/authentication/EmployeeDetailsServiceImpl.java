package com.organization.management.service.authentication;

import com.organization.management.core.models.Employee;
import com.organization.management.core.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EmployeeDetailsServiceImpl implements UserDetailsService {
  private static final String ERROR_MESSAGE = "Wrong username or PIN ";
  private final EmployeeRepository employeeRepository;

  public EmployeeDetailsServiceImpl(EmployeeRepository employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<Employee> employee = employeeRepository.findByUsername(username);

    if (employee.isEmpty()) {
      log.error(ERROR_MESSAGE + username);
      throw new UsernameNotFoundException(ERROR_MESSAGE);
    }

    return grantAuthority(employee.get());
  }

  private UserDetails grantAuthority(Employee employee) {
    List<GrantedAuthority> authorities = new ArrayList<>();
    SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + employee.getPosition().name());

    authorities.add(authority);
    return new org.springframework.security.core.userdetails.User(
        employee.getUsername(),
        employee.getPassword(),
        employee.isEnabled(),
        true,
        true,
        true,
        authorities);
  }
}
