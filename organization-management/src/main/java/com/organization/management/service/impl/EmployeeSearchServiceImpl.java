package com.organization.management.service.impl;

import com.organization.management.core.models.Employee;
import com.organization.management.core.repository.EmployeeRepository;
import com.organization.management.service.DepartmentService;
import com.organization.management.service.EmployeeSearchService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeSearchServiceImpl implements EmployeeSearchService {
  private final EmployeeRepository employeeRepository;
  private final EmployeeSharedServiceImpl employeeSharedService;
  private final DepartmentService departmentService;

  public EmployeeSearchServiceImpl(EmployeeRepository employeeRepository,
                                   EmployeeSharedServiceImpl employeeSharedService,
                                   DepartmentService departmentService) {
    this.employeeRepository = employeeRepository;
    this.employeeSharedService = employeeSharedService;
    this.departmentService = departmentService;
  }

  @Override
  public List<Employee> getEmployeesForTheDirector(String directorName) {
    return employeeRepository.findEmployeesByDirector(directorName);
  }

  @Override
  public List<Employee> getEmployeesForTheHeadOfDepartment(String headOfDepartmentName) {
    return employeeRepository.findEmployeesByHeadOfDepartment(headOfDepartmentName);
  }

  @Override
  public List<Employee> getAllEmployeesInPage(Pageable pageable) {
    Page<Employee> employeesPage = employeeRepository.findAll(pageable);
    return employeesPage.getContent();
  }

  @Override
  public Employee getEmployeeById(Long employeeId) {
    return employeeSharedService.findById(employeeId);
  }

  @Override
  public Employee findByUsername(String username) {
    return employeeSharedService.findByUsername(username);
  }
}
