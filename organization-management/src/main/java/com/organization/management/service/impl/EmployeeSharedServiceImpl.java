package com.organization.management.service.impl;

import com.organization.management.core.enums.PositionEnum;
import com.organization.management.core.models.Department;
import com.organization.management.core.models.Employee;
import com.organization.management.core.repository.EmployeeRepository;
import com.organization.management.web.exception.custom.EmployeeNotFoundException;
import com.organization.management.web.exception.custom.UnauthorizedActionException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * This is a shared service used by EmployeeUpdateService, EmployeeSearchService,
 * and EmployeeCreationService. It contains common methods and functionality
 * that are used across these services.
 */
@Service
public class EmployeeSharedServiceImpl {
  private static final String DIRECTOR_UNAUTHORIZED = "The specified Director does not lead the department of the given employee";
  private static final String HEAD_UNAUTHORIZED = "The specified Head of Department does not lead the department of the given employee";
  private static final String HEAD_DENIED = "Not authorized to appoint new Head of Department.";
  private static final String DIRECTOR_DENIED = "Not authorized to appoint new Director.";
  private final EmployeeRepository employeeRepository;

  public EmployeeSharedServiceImpl(EmployeeRepository employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  Employee findById(Long id) {
    return employeeRepository.findById(id)
        .orElseThrow(() -> new EmployeeNotFoundException(id));
  }

  Employee findByUsername(String username) {
    return employeeRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException(username));
  }

  void validateDirectorOfDepartment(Department department, Employee director) {
    if (!department.getDirectorate().getDirectorOfDirectorate().equals(director)) {
      throw new UnauthorizedActionException(DIRECTOR_UNAUTHORIZED);
    }
  }

  void validateHeadOfDepartment(Employee headOfDepartment, Department department) {
    if (!department.getHeadOfDepartment().equals(headOfDepartment)) {
      throw new UnauthorizedActionException(HEAD_UNAUTHORIZED);
    }
  }

  void validateIfNewEmployeeIsNotHeadOfDepartment(String position) {
    PositionEnum employeePosition = PositionEnum.valueOf(position);
    if (PositionEnum.HEAD_OF_DEPARTMENT.equals(employeePosition)) {
      throw new IllegalArgumentException(HEAD_DENIED);
    }
  }

  void validateIfNewEmployeeIsNotDirector(String position) {
    PositionEnum employeePosition = PositionEnum.valueOf(position);
    if (PositionEnum.DIRECTOR_OF_DIRECTORATE.equals(employeePosition)) {
      throw new IllegalArgumentException(DIRECTOR_DENIED);
    }
  }

  Employee findEmployeeByHeadAndId(String headName, Long id) {
    return employeeRepository.findEmployeeByHeadAndId(headName, id)
        .orElseThrow(() -> new EmployeeNotFoundException(id));
  }

  Employee findEmployeeByDirectorAndId(Long employeeId, String directorName) {
    return employeeRepository.findEmployeeByDirectorAndId(directorName, employeeId)
        .orElseThrow(() -> new EmployeeNotFoundException(employeeId));
  }
}
