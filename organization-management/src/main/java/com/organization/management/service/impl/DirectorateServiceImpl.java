package com.organization.management.service.impl;

import com.organization.management.core.models.Directorate;
import com.organization.management.core.models.Employee;
import com.organization.management.core.repository.DirectorateRepository;
import com.organization.management.service.DirectorateService;
import com.organization.management.web.dto.directorate.DirectorateFormDTO;
import com.organization.management.web.exception.custom.DirectorateNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DirectorateServiceImpl implements DirectorateService {
  private final DirectorateRepository directorateRepository;
  private final EmployeeSharedServiceImpl employeeSharedService;

  public DirectorateServiceImpl(DirectorateRepository directorateRepository,
                                EmployeeSharedServiceImpl employeeSharedService) {
    this.directorateRepository = directorateRepository;
    this.employeeSharedService = employeeSharedService;
  }

  @Override
  public Directorate findById(Long directorateId) {
    return findDirectorateById(directorateId);
  }

  @Override
  public Directorate createDirectorate(DirectorateFormDTO directorateFormDTO) {
    String employeeName = directorateFormDTO.getDirectorOfDirectorate().getUsername();
    Employee futureDirector = employeeSharedService.findByUsername(employeeName);

    Directorate directorate = buildDirectorate(futureDirector, directorateFormDTO);
    directorateRepository.save(directorate);
    return directorate;
  }

  @Override
  public Directorate updateDirectorate(Long directorateId, DirectorateFormDTO directorateFormDTO) {
    String employeeName = directorateFormDTO.getDirectorOfDirectorate().getUsername();
    Employee futureDirector = employeeSharedService.findByUsername(employeeName);

    Directorate directorate = findDirectorateById(directorateId);
    Directorate updatedDirectorate = applyUpdatesToDirectorate(directorate, futureDirector, directorateFormDTO);
    directorateRepository.save(updatedDirectorate);
    return updatedDirectorate;
  }

  @Override
  public void deleteDirectorate(Long directorateId) {
    directorateRepository.deleteById(directorateId);
  }

  private Directorate buildDirectorate(Employee futureDirector, DirectorateFormDTO directorateFormDTO) {
    Directorate directorate = new Directorate();
    directorate.setDirectorOfDirectorate(futureDirector);
    directorate.setName(directorateFormDTO.getName());
    directorate.setDescription(directorateFormDTO.getDescription());
    return directorate;
  }

  private Directorate applyUpdatesToDirectorate(Directorate directorate,
                                                Employee futureDirector,
                                                DirectorateFormDTO directorateFormDTO) {
    directorate.setDirectorOfDirectorate(futureDirector);
    directorate.setName(directorateFormDTO.getName());
    directorate.setDescription(directorateFormDTO.getDescription());
    return directorate;
  }

  private Directorate findDirectorateById(Long id) {
    return directorateRepository.findById(id)
        .orElseThrow(() -> new DirectorateNotFoundException(id));
  }
}
