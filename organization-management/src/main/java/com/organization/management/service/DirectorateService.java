package com.organization.management.service;

import com.organization.management.core.models.Directorate;
import com.organization.management.web.dto.directorate.DirectorateFormDTO;

public interface DirectorateService {
  Directorate findById(Long directorateId);

  Directorate updateDirectorate(Long directorateId, DirectorateFormDTO directorateFormDTO);

  Directorate createDirectorate(DirectorateFormDTO directorateFormDTO);

  void deleteDirectorate(Long directorateId);
}
