INSERT INTO directorate (name, description)
VALUES ('FIRST_DIRECTORATE', 'FIRST_DIRECTORATE');

INSERT INTO department (name, description, directorate_id)
VALUES ('FIRST_DEPARTMENT', 'FIRST_DEPARTMENT', 1);

-- Password is the same as the username

INSERT INTO employee (name, surname, username, personal_identification_number, age, position, department_id, is_enabled)
VALUES ('John', 'Doe','headFirst', '$2a$12$KGIZYpsSwYgtU.7UO9/99.2uMtM6oxVcfjVsWXAe4y9rsEh33EIEW', 20, 'HEAD_OF_DEPARTMENT', 1, true),
	   ('Jane', 'Doe','directorOne', '$2a$12$AdJOo/HNo.tyhquF8C/VkOONckAes92IIEuaWOKZhR.sRk.VGjimy', 20, 'DIRECTOR_OF_DIRECTORATE', 1, true);

UPDATE directorate SET director_of_directorate_id = 2 WHERE id = 1;
UPDATE department SET head_of_department_id = 1 WHERE id = 1;

INSERT INTO employee (name, surname, username, personal_identification_number, age, position, department_id, is_enabled)
VALUES ('Will', 'Smith', 'employee3', '$2a$12$BBGloH7sVMI1U9.ybJJsfO5FH4KO08sxB6/BsABAXePzK.TgnKdj2', 20, 'EMPLOYEE', 1, true),
 	   ('Bob', 'Marley', 'employee4', '$2a$12$TGExkekAIdMMAwwdxAx/.eA6knV9z6bzGn2J.VbXu26ah3GIVTFaa', 20, 'EMPLOYEE', 1, true),
 	   ('Donald', 'Trump', 'employee5', '$2a$12$s2RwbyBhtVNJJ9lImiguK.GvAm9DSxYb8kuZv2IzHrUKn4GYPRalu', 20, 'EMPLOYEE', 1, true);


-- Create Second Department under the same Directorate with Employees

INSERT INTO department (name, description, directorate_id)
VALUES ('SECOND_DEPARTMENT', 'SECOND_DEPARTMENT', 1);

INSERT INTO employee (name, surname, username, personal_identification_number, age, position, department_id, is_enabled)
VALUES ('Test', 'Doe','headSecond', '$2a$12$gz2NWmhjrDw8ulAVmWhGM.H.nt9om5z2BrdNZRmCpVEAE3pZ7XUUa', 20, 'HEAD_OF_DEPARTMENT', 2, true);

UPDATE department SET head_of_department_id = 6 WHERE id = 2;

INSERT INTO employee (name, surname, username, personal_identification_number, age, position, department_id, is_enabled)
VALUES ('Vladimir', 'Kez', 'employee7', '$2a$12$nZpBOpy6O6yfU4jQmf1wc.jbjQrpQ147Nqkpb4hfxjgmNanBm28V6', 20, 'EMPLOYEE', 2, true),
 	   ('Kez', 'Vladimir', 'employee8', '$2a$12$dhoAO5kGXvOZUFCgQTvGZO7Kte120BwfgA4gkgj4DS0lcEnJLG.UO', 20, 'EMPLOYEE', 2, true);


-- Create Second Directorate with a Department and Employees

INSERT INTO directorate (name, description)
VALUES ('SECOND_DIRECTORATE', 'SECOND_DIRECTORATE');

INSERT INTO department (name, description, directorate_id)
VALUES ('THIRD_DEPARTMENT', 'THIRD_DEPARTMENT', 2);

-- Password is the same as the username

INSERT INTO employee (name, surname, username, personal_identification_number, age, position, department_id, is_enabled)
VALUES ('Alice', 'Johnson','headThird', '$2a$12$bkW3RKQY8BbXJM1OXi04He2oz7655GAoIgp2kCD2pCFCg2b5Xtb.m', 20, 'HEAD_OF_DEPARTMENT', 3, true),
	   ('Bob', 'Smith','directorTwo', '$2a$12$D6/Kcd.yBvrGRT.xHPieyuAG3811Pb4hBMlAjxM3fhrZVWO3HDdEu', 20, 'DIRECTOR_OF_DIRECTORATE', 3, true);

UPDATE directorate SET director_of_directorate_id = 10 WHERE id = 2;
UPDATE department SET head_of_department_id = 9 WHERE id = 3;

INSERT INTO employee (name, surname, username, personal_identification_number, age, position, department_id, is_enabled)
VALUES ('Charlie', 'Brown', 'employee11', '$2a$12$6x9BQOf9lu3k3xLfc4P1U./LToivbU7aKTsPFUfhbH5cNRJWvmvOu', 20, 'EMPLOYEE', 3, true),
 	   ('Daisy', 'Green', 'employee12', '$2a$12$wfyCnrkoOX9lp4qjWE/Pk.CxiZ.HzUo/PGc9zIlg6CAdaf8K/vBau', 20, 'EMPLOYEE', 3, true);


-- ADMIN Import

INSERT INTO employee (name, surname, username, personal_identification_number, age, position, department_id, is_enabled)
VALUES ('Admin', 'User', 'admin', '$2a$12$zal4cBH3ym3.IBp.ROW4uOrInDDK6Ohy6D1VpJbpiRbE2urXwAhwK', 30, 'ADMIN', 1, true);